import java.util.ArrayList;

public class equipos {
	private int id;
	private String nombre;
	private int vidas;
	private int[] ataques = new int[equipos];
	private int defensa;
	private int dano = 0;
	private boolean muerto;
	private int id_planeta;
	private int misilslimit;

	public static int equipos = 0;
	private int limitturno = 0;
	public static ArrayList<String> resumen;// new

	// constructor (punto 2 del menu crear equipos)
	equipos(String nombre) {
		this.nombre = nombre;
		planetas();
	}

	public boolean isMuerto() {
		return muerto;
	}

	public void setMuerto(boolean muerto) {
		this.muerto = muerto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getVidas() {
		return vidas;
	}

	public void setVidas(int vidas) {
		this.vidas = vidas;
	}

	public int[] getAtaques() {
		return ataques;
	}

	public void setAtaques(int[] ataques) {
		this.ataques = ataques;
	}

	public int getDefensa() {
		return defensa;
	}

	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}

	public void setid_planeta(int id) {
		this.id_planeta = id;
	}

	public int getid_planeta() {
		return id_planeta;
	}

	public int getMisilslimit() {
		return misilslimit;
	}

	public void setmisilslimit(int misilslimit) {
		this.misilslimit = misilslimit;
	}

	// metodos
	public void Reset() {
		for (int a = 0; a < ataques.length; a++) {
			ataques[a] = 0;
			dano = 0;
		}
	}

	public boolean Ataque(int idPlaneta, int valor, int misilLimit, int y) {
		// cuenta misiles que ya has usado y si sumandolos a los nuevos da mas del
		// limite
		// error
		int misil = 0;
		for (int i = 0; i < ataques.length; i++) {
			misil = misil + ataques[i];
		}

		misil = misil - ataques[idPlaneta];
		if (misil + valor > misilLimit) {
			System.out.println("Error, no te quedan tantos misiles. Te quedan " + (misilLimit - misil)
					+ " misiles. Vuelve a introducir misiles: (si no te quedan misiles pon 0) ");
			return true;
		} else {
			ataques[idPlaneta] = valor;
			return false;
		}
	}

	public void Defensa() {
		// para saber cuantos misiles van a defensa se resta 50 menos los misiles
		// lanzados por el equipo a otros equipos
		for (int a = 0; a < ataques.length; a++) {
			defensa = (defensa - ataques[a]);
		}
	}

	public void Defensa2() {
		System.out.println("\nDefensa: " + defensa + "\n");
		defensa = defensa / 2;
	}

	// en combate paso los arrays con los misiles lanzados de los otros equipos y se
	// resta con la defensa y la vida
	public void Combate(int ataque, String atacante) {
		
		dano += ataque;
		
		//para mostrar despues por pantalla
		if(ataque > 0) {
			//Esto peta mu fuerte-------------------------------------------------------------------------------------------
			resumen.add(resumen(atacante,ataque));
		}

		limitturno++;
		// al pasar el ultimo equipo(limitturno) se resta el dano a la defensa y despues
		// a la vida
		if (limitturno == equipos) {
			dano -= defensa;
			if (dano > 0) {
				vidas = vidas - dano;

			}
			dano = 0;
			limitturno = 0;
			System.out.println("-----------------------------siguiente equipo--------------------------");
		}


		if (vidas <= 0) {
			vidas = 0;
		}
	}

	public void muerte() { // indica si un jugador a muerto
		if (vidas <= 0) {
			if (muerto == false) {
				equipos--;
			}
			muerto = true;
		}
	}

	public void planetas() {
		if (this.id == 1) {
			this.setVidas(200);
			this.setmisilslimit(50);
		} else if (this.id == 2) {
			this.setVidas(200);
			this.setmisilslimit(50);
		} else if (this.id == 3) {
			this.setVidas(200);
			this.setmisilslimit(50);
		} else if (this.id == 4) {
			this.setVidas(200);
			this.setmisilslimit(50);
		} else if (this.id == 5) {
			this.setVidas(200);
			this.setmisilslimit(50);
		} else if (this.id == 6) {
			this.setVidas(200);
			this.setmisilslimit(50);
		} else if (this.id == 7) {
			this.setVidas(400);
			this.setmisilslimit(10);
		} else if (this.id == 8) {
			this.setVidas(100);
			this.setmisilslimit(50);
		} else if (this.id == 9) {
			this.setVidas(200);
			this.setmisilslimit(50);
		} else if (this.id == 10) {
			this.setVidas(200);
			this.setmisilslimit(50);
		}
	}
public String resumen(String nombre, int ataque ) {
	String x = "el equipo "+ nombre + "ha atacado a "+this.nombre+ " con "+ ataque;
return x;
}
}
